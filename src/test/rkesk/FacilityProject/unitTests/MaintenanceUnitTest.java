package test.rkesk.FacilityProject.unitTests;


import org.junit.Test;
import static org.junit.Assert.*;
import java.time.LocalDate;
import com.rskesk.FacilityProject.model.Maintenance.*;
import com.rskesk.FacilityProject.model.Maintenance.MaintenanceStatus;

public class MaintenanceUnitTest {
	
	MaintenanceHandler test = new MaintenanceHandler();
	//MaintenanceRequest  tmp = new MaintenanceRequest("TEST");
	
	@Test
	public void testMakeFacilityMaintRequest(){
		test.makeFacilityMaintRequest("TEST0", null);
		assertEquals(LocalDate.now(), test.getRequestAt(0).getDateOfRequest());
		assertEquals("TEST0", test.getRequestAt(0).getDescription());
		assertEquals(1,test.getMaintenanceRequests().size());
		
		test.makeFacilityMaintRequest("TEST1", null);
		assertEquals(LocalDate.now(), test.getRequestAt(1).getDateOfRequest());
		assertEquals("TEST1", test.getRequestAt(1).getDescription());
		assertEquals(2,test.getMaintenanceRequests().size());
		
	}
	
	@Test
	public void testMaintStatus(){
		//ACCEPTED status 
		test.makeFacilityMaintRequest("TEST0", null);
		test.makeFacilityMaintRequest("TEST1", null);
		//assertEquals(2,test.getMaintenanceRequests().size());
		assertFalse(test.getRequestAt(0).getMaintainenceOrder().getStatus().equals(MaintenanceStatus.ACCEPTED));
		test.getRequestAt(0).getMaintainenceOrder().setStatus(MaintenanceStatus.ACCEPTED);
		assertTrue(test.getRequestAt(0).getMaintainenceOrder().getStatus().equals(MaintenanceStatus.ACCEPTED));
		
		//SCHEDULED status
		assertEquals(false, test.getRequestAt(0).getMaintainenceOrder().getStatus().equals(MaintenanceStatus.SCHEDULED));
		test.getRequestAt(0).getMaintainenceOrder().setStatus(MaintenanceStatus.SCHEDULED);
		assertEquals(true, test.getRequestAt(0).getMaintainenceOrder().getStatus().equals(MaintenanceStatus.SCHEDULED));
		
		// COMPLETE Status
		assertEquals(false, test.getRequestAt(0).getMaintainenceOrder().getStatus().equals(MaintenanceStatus.COMPLETED));
		test.getRequestAt(0).getMaintainenceOrder().setStatus(MaintenanceStatus.COMPLETED);
		assertEquals(true,test.getRequestAt(0).getMaintainenceOrder().getStatus().equals(MaintenanceStatus.COMPLETED));
	}
	
	
	@Test
	public void testCalcMaintCostForOrder(){
		test.makeFacilityMaintRequest("TEST0", null);
		test.getRequestAt(0).getMaintainenceOrder().setCost(new Cost(30,12.34,67.89));
		test.getRequestAt(0).getMaintainenceOrder().getCost().findOrderCost();
		assertTrue(395.23 == test.getRequestAt(0).getMaintainenceOrder().getCost().getOrderCost());
	}

@Test
public void testCalcMaintCostForFacility() {
	//SET UP 
	test.makeFacilityMaintRequest("TEST0", null);
	test.makeFacilityMaintRequest("TEST1", null);
	test.makeFacilityMaintRequest("TEST2", null);
	
	test.getRequestAt(0).getMaintainenceOrder().setCost(new Cost(23, 34.56, 78.90));
	test.getRequestAt(1).getMaintainenceOrder().setCost(new Cost(1, 2.00, 45.67));
	test.getRequestAt(2).getMaintainenceOrder().setCost(new Cost(67, 34.76, 455.67));
	
	assertEquals(354.96, test.getRequestAt(0).getMaintainenceOrder().getCost().getOrderCost(), .1);
	assertEquals(58.17 == test.getRequestAt(1).getMaintainenceOrder().getCost().getOrderCost(), .1);
	assertEquals(1193.93 == test.getRequestAt(2).getMaintainenceOrder().getCost().getOrderCost(), .1);
	
	// TEST
	//395.23+354.96+58.17+1193.93
	assertTrue(2002.29==test.calcMaintenanceCostForFacility());
	
}
//	
//	@Test
//	public void calcDownTimeForFacility(){
//		
//	}
//	
//	@Test
//	public void listMaintRequests(){
//		
//	}
//	
//	@Test
//	public void listMaintenance(){
//		
//	}
//	
//	@Test
//	public void listFacilityProblems(){
//		
//	}
}
