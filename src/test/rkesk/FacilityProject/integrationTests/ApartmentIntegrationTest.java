package test.rkesk.FacilityProject.integrationTests;

import static org.junit.Assert.*;

import com.rskesk.FacilityProject.model.Facility.ApartmentBuilding;
import com.rskesk.FacilityProject.model.Facility.ApartmentUnit;
import com.rskesk.FacilityProject.model.Facility.FacilityInformation;
import com.rskesk.FacilityProject.model.Facility.FacilityInformationHandler;

public class ApartmentIntegrationTest {

	@org.junit.Test
	public void testAddingNewApartmentUnitToApartmentBuildingCopiesStreetAddress() throws Exception {
		FacilityInformation apartmentfI = new FacilityInformationHandler();
		apartmentfI.setFacilityStreetAddress("123 Main St");
		
		ApartmentBuilding apartmentBuilding = new ApartmentBuilding(apartmentfI);
		
		ApartmentUnit unit = new ApartmentUnit();
		apartmentBuilding.addNewFacility(unit);
		
		assertEquals(apartmentBuilding.getFacilityInformation().getFacilityStreetAddress(), 
						apartmentBuilding.getApartmentUnits().get(0).getFacilityInformation().getFacilityStreetAddress());
	}

}
