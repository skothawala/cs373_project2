package com.rskesk.FacilityProject.client;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.rskesk.FacilityProject.model.Facility.ApartmentBuilding;
import com.rskesk.FacilityProject.model.Facility.ApartmentUnit;
import com.rskesk.FacilityProject.model.Facility.Facility;
import com.rskesk.FacilityProject.model.Facility.FacilityInformation;
import com.rskesk.FacilityProject.model.Facility.User.FacilityUser;

public class SpringClient {

	public static void main(String[] args) throws Exception {
		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
		System.out.println("***************** Application Context instantiated! ******************");

		ApartmentBuilding lakeSideApartmentComplexes = (ApartmentBuilding) context.getBean("apartmentBuilding");
		
		FacilityInformation apartmentfI = lakeSideApartmentComplexes.getFacilityInformation();
		apartmentfI.setFacilityStreetAddress("123 Main St");
		apartmentfI.setFacilityCity("Chicago");
		apartmentfI.setFacilityName("LakeSide Apartment Complex");
		apartmentfI.setFacilityState("IL");
		apartmentfI.setFacilityZip("60659");
		
		
		ApartmentUnit unit1 = (ApartmentUnit) context.getBean("apartmentUnit");
		
		FacilityUser saad = (FacilityUser) context.getBean("facilityUser");
		saad.setFirstName("Saad");
		saad.setLastName("K");
		saad.setSsn("...");
		
		FacilityUser katie = (FacilityUser) context.getBean("facilityUser");
		katie.setFirstName("Katie");
		katie.setLastName("E");
		katie.setSsn("...");
		
		FacilityUser ryan = (FacilityUser) context.getBean("facilityUser");
		ryan.setFirstName("Ryan");
		ryan.setLastName("S");
		ryan.setSsn("...");
		
		
		
		unit1.getFacilityUses().getCurrentFacilityUse().setUseDescription("Living");
		unit1.getFacilityUses().getCurrentFacilityUse().setUser(saad);
		unit1.getFacilityUses().vacateFacility();
		
		unit1.getFacilityUses().assignFacilityToUse(katie, "Investment");
		unit1.getFacilityUses().vacateFacility();
		
		unit1.getFacilityUses().assignFacilityToUse(saad, "Living");
		unit1.setApartmentName("Saad's Apt");
		lakeSideApartmentComplexes.addNewFacility(unit1);
		
		
		ApartmentUnit unit2 = (ApartmentUnit) context.getBean("apartmentUnit");
		
		unit2.getFacilityUses().getCurrentFacilityUse().setUseDescription("Investment");
		unit2.getFacilityUses().getCurrentFacilityUse().setUser(katie);
		unit2.setApartmentName("Katie's Apt");
		lakeSideApartmentComplexes.addNewFacility(unit2);
		
		ApartmentUnit unit3 = (ApartmentUnit) context.getBean("apartmentUnit");
		unit3.getFacilityUses().getCurrentFacilityUse().setUseDescription("Partying");
		unit3.getFacilityUses().getCurrentFacilityUse().setUser(ryan);
		unit3.setApartmentName("Ryan's Apt");
		lakeSideApartmentComplexes.addNewFacility(unit3);
		
		ApartmentUnit pentHouse = (ApartmentUnit) context.getBean("apartmentUnit");
		pentHouse.setApartmentName("PentHouse");
		lakeSideApartmentComplexes.addNewFacility(pentHouse);
		
		
		
		unit2.getFacilityMaintenence().makeFacilityMaintRequest("Shower not working", unit2.getFacilityUses().getCurrentFacilityUse());
		unit2.getFacilityMaintenence().acceptMaintenance(true);
		unit2.getFacilityMaintenence().scheduleMaintenance();
		unit2.getFacilityMaintenence().completeMaintainence();
		
		unit3.getFacilityMaintenence().makeFacilityMaintRequest("Faucet not working", unit3.getFacilityUses().getCurrentFacilityUse());
		unit3.getFacilityMaintenence().acceptMaintenance(true);
		
		unit3.getFacilityMaintenence().makeFacilityMaintRequest("Door not closing", unit3.getFacilityUses().getCurrentFacilityUse());
		unit3.getFacilityMaintenence().scheduleMaintenance();
		
		
		
		/***************END SETUP***************/
		
		
		System.out.println("Facility Name:\t\t" + lakeSideApartmentComplexes.getFacilityInformation().getFacilityName());
		System.out.println("Apartment Address:\t" + lakeSideApartmentComplexes.getFacilityInformation().getFacilityStreetAddress());
		System.out.println("\t\t\t" + lakeSideApartmentComplexes.getFacilityInformation().getFacilityCity() + ", " 
					+ lakeSideApartmentComplexes.getFacilityInformation().getFacilityState() + " "
					+ lakeSideApartmentComplexes.getFacilityInformation().getFacilityZip());
		
		System.out.println("Number of Units: " + lakeSideApartmentComplexes.getApartmentUnits().size());
		System.out.println("Facility Use Rate: " + lakeSideApartmentComplexes.calcUsageRate(true) + "%");
		
		
		System.out.println("List of Apartments:");
		for(Facility apartment : lakeSideApartmentComplexes.getApartmentUnits()) {
			System.out.println("*********************************************");
			System.out.println("\t\tName: " + apartment.getFacilityInformation().getFacilityName());
			
			if(apartment.getFacilityUses().isFacilityInUse()) {
				System.out.println("\t\tFacility is used for: " + apartment.getFacilityUses().getCurrentFacilityUse().getUseDescription());
				System.out.println("\t\tFacility started being used: " + apartment.getFacilityUses().getCurrentFacilityUse().getStartUseDate());
				System.out.println("\t\tFacility is being used by: " + apartment.getFacilityUses().getCurrentFacilityUse().getFacilityUser());
				System.out.println("\t\tFacility Turnovers: " + apartment.calcTurnOver());
			} else {
				System.out.println("\t\tFacility Not In Use");
			}
					
			System.out.println("*********************************************");
		}
		
		System.out.println("\nFacility Problems:");
		lakeSideApartmentComplexes.getFacilityMaintenence().listFacilityProblems();
		System.out.println("\nFacility Cost\t" + lakeSideApartmentComplexes.getFacilityMaintenence().calcMaintenanceCostForFacility());
		
		lakeSideApartmentComplexes.listFacilities();
		System.out.println("\n\n\nMischief managed");
	}

}
