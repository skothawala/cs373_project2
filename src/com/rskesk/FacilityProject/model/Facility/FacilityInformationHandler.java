package com.rskesk.FacilityProject.model.Facility;

public class FacilityInformationHandler implements FacilityInformation{

	private String facilityName;
		
	private String facilityStreetAddress;
	private String facilityZip;
	private String facilityState;
	private String facilityCity;
	private String facilityType;
	

	@Override
	public void setFacilityName(String name) {
		this.facilityName = name;		
	}

	
	@Override
	public void setFacilityStreetAddress(String fSA) {
		this.facilityStreetAddress = fSA;		
	}

	@Override
	public void setFacilityZip(String zip) {
		this.facilityZip = zip;		
	}

	@Override
	public void setFacilityState(String state) {
		this.facilityState = state;		
	}

	@Override
	public void setFacilityCity(String city) {
		this.facilityCity = city;		
	}

	@Override
	public String getFacilityStreetAddress() {
		return this.facilityStreetAddress;
	}

	@Override
	public String getFacilityZip() {
		return this.facilityZip;
	}

	@Override
	public String getFacilityState() {
		return this.facilityState;
	}

	@Override
	public String getFacilityCity() {
		return this.facilityCity;
	}


	@Override
	public String getFacilityName() {
		return this.facilityName;
	}


	@Override
	public void setFacilityType(String type) {
		this.facilityType = type;
	}


	@Override
	public String getFacilityType() {
		return this.facilityType;
	}

}
