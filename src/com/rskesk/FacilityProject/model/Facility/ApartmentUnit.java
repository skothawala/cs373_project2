package com.rskesk.FacilityProject.model.Facility;

import com.rskesk.FacilityProject.model.Facility.Inspection.Inspectable;
import com.rskesk.FacilityProject.model.Facility.User.Usable;
import com.rskesk.FacilityProject.model.Facility.User.UsableHandler;
import com.rskesk.FacilityProject.model.Maintenance.Maintainable;
import com.rskesk.FacilityProject.model.Maintenance.MaintenanceHandler;

public class ApartmentUnit extends Facility {

	public ApartmentUnit() {


	}

	public void setApartmentName(String name) {
		this.facilityInformation.setFacilityName(name);
	}

	@Override
	public FacilityInformation getFacilityInformation() {
		return this.facilityInformation;
	}

	@Override
	public void addNewFacility(Facility f) throws Exception {
		throw new Exception("ApartmentUnit can not addNewFacility in addNewFacility");
	}

	@Override
	public boolean removeFacility(String facilityName) throws Exception {
		throw new Exception("ApartmentUnit can not removeFacility in removeFacility");
	}

	public void listFacilities() {
		System.out.println("ApartmentUnit has no child-facilities");
	}

	@Override
	public Inspectable getFacilityInspection() {
		return this.facilityInspection;
	}

	@Override
	public void setFacilityInspection(Inspectable fI) {
		this.facilityInspection = fI;
	}

	@Override
	public Usable getFacilityUses() {
		return this.facilityUses;
	}

	@Override
	public void setFacilityUses(Usable u) {
		this.facilityUses = u;
	}

	@Override
	public double calcUsageRate(boolean asPercent) {
		if (this.getFacilityUses().isFacilityInUse())
			return 1.0;
		return 0.0;
	}

	@Override
	public double calcTurnOver() {
		if (this.getFacilityUses().isFacilityInUse())
			return this.getFacilityUses().getIndividualFacilityUses().size() - 1;

		return this.getFacilityUses().getIndividualFacilityUses().size();
	}

	@Override
	public int requestAvailableCapacity() {
		if (this.getFacilityUses().isFacilityInUse())
			return 0;
		return 1;
	}

	public Maintainable getFacilityMaintainence(Maintainable m) {
		return this.facilityMaintainence = m;
	}

	@Override
	public void setFacilityInformation(FacilityInformation fI) {
		this.facilityInformation = fI;
	}

	@Override
	public void setFacilityMaintainence(Maintainable fI) {
		this.facilityMaintainence = fI;
	}

	@Override
	public Maintainable getFacilityMaintenence() {
		return this.facilityMaintainence;
	}

}
