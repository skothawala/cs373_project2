package com.rskesk.FacilityProject.model.Facility;

public interface FacilityInformation {
	void setFacilityName(String n);
	void setFacilityStreetAddress(String fSA);
	void setFacilityZip(String zip);
	void setFacilityState(String state);
	void setFacilityCity(String city);
	void setFacilityType(String type);
	
	String getFacilityName();
	String getFacilityStreetAddress();
	String getFacilityZip();
	String getFacilityState();
	String getFacilityCity();
	String getFacilityType();
}
