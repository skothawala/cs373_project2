package com.rskesk.FacilityProject.model.Facility.Inspection;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by keetz on 2/9/17.
 */
public class InspectionHandler implements Inspectable {
    
	private int frequencyOfInspectionInDays;
    private ArrayList<InspectionObjInterface> inspectionObjects;
    
    public InspectionHandler(){
    	this.frequencyOfInspectionInDays = 60;
    }
    
    public InspectionHandler(int frequency){
    	this.frequencyOfInspectionInDays = frequency;

    }
    
    public void setFrequencyOfInspectionInDays(int frequencyOfInsp){
        this.frequencyOfInspectionInDays=frequencyOfInsp;
    }
    
    public int getFrequency(){
        return frequencyOfInspectionInDays;
    }
    
    public ArrayList<InspectionObjInterface> getInspections() {
    	return this.inspectionObjects;
    }
    
    public void setInspectionObjects(ArrayList<InspectionObjInterface> is) {
    	this.inspectionObjects = is;
    }
    
	@Override
	public void listInspections() {
		for(InspectionObjInterface inspectionObject: this.getInspections()) {
			System.out.println("************************************");
			System.out.println(inspectionObject.getPrettyPrint());
			System.out.println("************************************");
		}
	}
	@Override
	public void scheduleInspection() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public LocalDate getNextInspectionDate() {
		if(this.getInspections().size() == 0)
			return LocalDate.now();
		
		return this.getInspections().get(this.getInspections().size() - 1).getDateOfInspection().plusDays(this.frequencyOfInspectionInDays);
	}


	@Override
	public void addInspection(InspectionObjInterface inspection) {
		this.inspectionObjects.add(inspection);
	}

}
