package com.rskesk.FacilityProject.model.Facility.Inspection;

import java.time.LocalDate;

public interface Inspectable {
	void listInspections();
	void scheduleInspection();
	LocalDate getNextInspectionDate();
	void addInspection(InspectionObjInterface inspection);
	
}
