package com.rskesk.FacilityProject.model.Facility.Inspection;

import java.time.LocalDate;

public class Inspection implements InspectionObjInterface{
	private boolean passedInsp;
	private LocalDate dateOfInspection;
	
	public Inspection() {
		this.passedInsp = false;
		this.dateOfInspection = LocalDate.now();
	}
	
	public Inspection(boolean result, LocalDate time) {
		this.passedInsp = result;
		this.dateOfInspection = time;
	}
	
	public void setPassedInsp(boolean result) {
		this.passedInsp = result;
	}
	
	public void setDateOfInspection(LocalDate d) {
		this.dateOfInspection = d;
	}
	
	public LocalDate getDateOfInspection() {
		return this.dateOfInspection;
	}
	
	public String getPrettyPrint() {
		return "Date of Inspection: " + this.dateOfInspection.getMonth() + "/" + this.dateOfInspection.getDayOfMonth() + "/"
				+ this.dateOfInspection.getYear() + "\n" + "Result: " + this.passedInsp; 
	}

}
