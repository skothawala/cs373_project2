package com.rskesk.FacilityProject.model.Facility.Inspection;

import java.time.LocalDate;

public interface InspectionObjInterface {
	public abstract void setPassedInsp(boolean result);
	public abstract void setDateOfInspection(LocalDate d);
	public abstract LocalDate getDateOfInspection();
	public abstract String getPrettyPrint();	
}
