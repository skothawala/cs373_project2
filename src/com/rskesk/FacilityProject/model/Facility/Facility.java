package com.rskesk.FacilityProject.model.Facility;

import java.util.ArrayList;

import com.rskesk.FacilityProject.model.Facility.Inspection.*;
import com.rskesk.FacilityProject.model.Facility.User.Usable;
import com.rskesk.FacilityProject.model.Maintenance.Maintainable;

/**
 * Created by keetz on 2/9/17.
 */
public abstract class Facility {
	
	protected FacilityInformation facilityInformation;
	public abstract void setFacilityInformation(FacilityInformation fI);
	public abstract FacilityInformation getFacilityInformation();
    
	
    protected Inspectable facilityInspection;
    public abstract Inspectable getFacilityInspection();
    public abstract void setFacilityInspection(Inspectable fI);
    
    
    
    protected Usable facilityUses;
    public abstract Usable getFacilityUses();
    public abstract void setFacilityUses(Usable u);
	public abstract double calcUsageRate(boolean asPercent);
	public abstract double calcTurnOver();
	

	protected Maintainable facilityMaintainence;
	public abstract void setFacilityMaintainence(Maintainable fI);
	public abstract Maintainable getFacilityMaintenence();
    
    public abstract int requestAvailableCapacity();

    public abstract void addNewFacility(Facility f) throws Exception;
    public abstract boolean removeFacility(String facilityName) throws Exception;
    public abstract void listFacilities();
	
    
    
   
	
    
    
}
