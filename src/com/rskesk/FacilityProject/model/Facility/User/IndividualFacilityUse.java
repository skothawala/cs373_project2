package com.rskesk.FacilityProject.model.Facility.User;

import java.time.LocalDate;

public class IndividualFacilityUse implements IndividualFacilityUseInterface {
	
	private FacilityUserInterface user;
	private LocalDate startUseDate;
	private LocalDate endUseDate;
	private String useDescription;
	
	public IndividualFacilityUse() {}
	
	public IndividualFacilityUse(FacilityUserInterface u, String useDescription) {
		this.user = u;
		this.useDescription = useDescription;
		this.startUseDate = LocalDate.now();
		this.endUseDate = null;
		
	}

	public IndividualFacilityUse(FacilityUserInterface u, String useDescription, LocalDate startDate) {
		this.user = u;
		this.useDescription = useDescription;
		this.startUseDate = startDate;
		this.endUseDate = null;
	}
	
	public IndividualFacilityUse(FacilityUserInterface u, String useDescription, LocalDate startDate, LocalDate endDate) {
		this.user = u;
		this.useDescription = useDescription;
		this.startUseDate = startDate;
		this.endUseDate = endDate;
	}
	
	public void endUse() {
		this.endUseDate = LocalDate.now();
	}
	
	public String getUseDescription() {
		return this.useDescription;
	}
	
	public FacilityUserInterface getFacilityUser() {
		return this.user;
	}
	
	public LocalDate getStartUseDate() {
		return this.startUseDate;
	}
	
	public LocalDate getEndUseDate() {
		return this.endUseDate;
	}
	
	public void setUser(FacilityUser fu)  {
		this.user = fu;
	}
	
	public void setStartUseDate(LocalDate d) {
		this.startUseDate = d;
	}
	
	public void setEndUseDate(LocalDate d) {
		this.endUseDate = d;
	}
	
	public void setUseDescription(String d) {
		this.useDescription = d;
	}
}
