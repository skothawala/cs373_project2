package com.rskesk.FacilityProject.model.Facility.User;

import java.util.ArrayList;

public interface Usable {
	boolean isInUseDuringInterval();
	void assignFacilityToUse(FacilityUserInterface fU, String useDescription) throws Exception;
	void vacateFacility();
	
	boolean isFacilityInUse();
	IndividualFacilityUseInterface getCurrentFacilityUse() throws Exception;
	
	ArrayList<IndividualFacilityUseInterface> getIndividualFacilityUses();
	void setIndividualFacilityUses(ArrayList<IndividualFacilityUseInterface> individualFacilityUses);
	
	
	
}
