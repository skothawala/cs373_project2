package com.rskesk.FacilityProject.model.Facility.User;

import java.time.LocalDate;

public interface IndividualFacilityUseInterface {
	public abstract void endUse();
	public abstract String getUseDescription();
	public abstract FacilityUserInterface getFacilityUser();
	public abstract LocalDate getStartUseDate();
	public abstract LocalDate getEndUseDate();
	public abstract void setUser(FacilityUser fu) ;
	public abstract void setStartUseDate(LocalDate d);
	public abstract void setEndUseDate(LocalDate d);
	public abstract void setUseDescription(String d);
}
