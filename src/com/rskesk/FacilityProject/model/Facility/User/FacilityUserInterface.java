package com.rskesk.FacilityProject.model.Facility.User;

public interface FacilityUserInterface {
	public abstract String getFirstName();
	public abstract void setFirstName(String firstName);
	public abstract String getLastName();
	public abstract void setLastName(String lastName);
	public abstract String getSsn();
	public abstract void setSsn(String ssn);
}
