package com.rskesk.FacilityProject.model.Maintenance;

import java.time.LocalDate;

import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUseInterface;

public interface MaintenanceRequestInterface {
	public abstract MaintenanceOrderInterface getMaintainenceOrder();
	public abstract void setMaintenanceOrder(MaintenanceOrderInterface maintenanceOrder);
	public abstract String getDescription();
	public abstract void setDescription(String description);
	public abstract LocalDate getDateOfRequest();
	public abstract void setDateOfRequest(LocalDate dateOfRequest);
	public abstract IndividualFacilityUseInterface getRequestor();
	public abstract void setRequestor(IndividualFacilityUseInterface r);
}
