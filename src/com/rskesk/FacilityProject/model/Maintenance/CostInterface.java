package com.rskesk.FacilityProject.model.Maintenance;

public interface CostInterface {
	public abstract void findOrderCost();
	public abstract void addNewSupplyUsed(double newSupply);
	public abstract void addNewSupplyPurch(double newSupply);
	public abstract double getOrderCost() ;
	public abstract void setOrderCost(double orderCost) ;
	public abstract int getHoursOnJob() ;
	public abstract void setHoursOnJob(int hoursOnJob) ;
	public abstract double getSuppliesUsed() ;
	public abstract void setSuppliesUsed(double suppliesUsed) ;
	public abstract double getSuppliesPurchased() ;
	public abstract void setSuppliesPurchased(double suppliesPurchased) ;
	public abstract double getEmpHourlyRate();
}
