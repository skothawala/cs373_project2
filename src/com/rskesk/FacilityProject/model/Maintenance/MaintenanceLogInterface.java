package com.rskesk.FacilityProject.model.Maintenance;

import java.time.LocalDate;

public interface MaintenanceLogInterface {
	public abstract String getDescription();
	public abstract void setDescription(String description);
	public abstract LocalDate getDate();
	public abstract void setDate(LocalDate date);
}
