package com.rskesk.FacilityProject.model.Maintenance;

import java.time.LocalDate;
import java.util.ArrayList;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.rskesk.FacilityProject.model.Facility.User.FacilityUser;
import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUse;
import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUseInterface;
import com.rskesk.FacilityProject.model.Maintenance.MaintenanceRequest;;

public class MaintenanceHandler implements Maintainable{

	private ArrayList<MaintenanceRequestInterface> maintenanceRequests;
	private ArrayList<MaintenanceLogInterface> maintenanceLogs;

	
	public MaintenanceHandler() {
	}
	
	
	
	public ArrayList<MaintenanceRequestInterface> getMaintenanceRequests() {
		return maintenanceRequests;
	}
	public void setMaintenanceRequests(ArrayList<MaintenanceRequestInterface> maintenanceRequests) {
		this.maintenanceRequests = maintenanceRequests;
	}
	
	public MaintenanceRequestInterface getRequestAt(int i){
		return maintenanceRequests.get(i);
	}
	
	public void setMaintenanceLogs(ArrayList<MaintenanceLogInterface> mL) {
		this.maintenanceLogs = mL;
	}


	@Override
	public void makeFacilityMaintRequest(String desc, IndividualFacilityUseInterface f) {
		
		MaintenanceRequestInterface mr = ((MaintenanceRequestInterface) (new ClassPathXmlApplicationContext("META-INF/app-context.xml")).getBean("maintenanceRequest"));
		
		mr.setDescription(desc);
		mr.setRequestor(f);
		mr.setMaintenanceOrder(new MaintenanceOrder());
		this.maintenanceRequests.add(mr);
			
	}

	@Override
	public void scheduleMaintenance() {
		// TODO Goes through all of the requests in array list, need a checker for a completed or scheduled item
		for (int i =0; i<maintenanceRequests.size(); i++){
			if(this.maintenanceRequests.get(i).getMaintainenceOrder().getStatus().equals(MaintenanceStatus.ACCEPTED)){
				this.maintenanceRequests.get(i).getMaintainenceOrder().setStatus(MaintenanceStatus.SCHEDULED);
				this.maintenanceLogs.add(new MaintenanceLog(LocalDate.now(), "Scheduled " + this.maintenanceRequests.get(i).getDescription()));
			}			
		}
	}
	
	@Override
	public void acceptMaintenance(boolean empAssigned){
		//TODO: checks alllllll for the items in the list, there could be a better/ different way to to do this
		for (int i =0; i<maintenanceRequests.size(); i++){
			if(this.maintenanceRequests.get(i).getMaintainenceOrder().getStatus().equals(MaintenanceStatus.PENDING)){
				this.maintenanceRequests.get(i).getMaintainenceOrder().setStatus(MaintenanceStatus.ACCEPTED);
				this.maintenanceLogs.add(new MaintenanceLog(LocalDate.now(), "ACCEPTED " + this.maintenanceRequests.get(i).getDescription()));
			}			

		}
	}

	@Override
	public void completeMaintainence(){
		// TODO Goes through all of the requests in array list, need a checker for a completed item
		for (int i =0; i<maintenanceRequests.size(); i++){
			if(this.maintenanceRequests.get(i).getMaintainenceOrder().getStatus().equals(MaintenanceStatus.SCHEDULED)){
				this.maintenanceRequests.get(i).getMaintainenceOrder().setStatus(MaintenanceStatus.COMPLETED);
				this.maintenanceLogs.add(new MaintenanceLog(LocalDate.now(), "COMPLETED " + this.maintenanceRequests.get(i).getDescription()));
			}
		}
	}
	
	@Override 
	public double calcMaintenanceCostForOrder(){
		double order = 0;
		for (int i =0; i<maintenanceRequests.size(); i++){
			order = this.maintenanceRequests.get(i).getMaintainenceOrder().getCost().getOrderCost();
		}
		return order;
		
	}
	@Override
	public double calcMaintenanceCostForFacility() {
		double facil =0;
		for (int i =0; i<maintenanceRequests.size(); i++){
			facil = facil+ this.maintenanceRequests.get(i).getMaintainenceOrder().getCost().getOrderCost();
		}
		return facil;
	}

	@Override
	public double calcProblemRateForFacility() {		
		return this.maintenanceRequests.size();
	}

	@Override
	public double calcDownTimeForFacility() {
		return 0.0;		
	}

	@Override
	public void listMaintRequests() {
		//TODO: what attribute should this print out???
		for (int i =0; i< maintenanceRequests.size(); i++){
			System.out.println(maintenanceRequests.get(i));
		}
	}

	@Override
	public void listMaintenance() {
		for(MaintenanceRequestInterface mr: this.getMaintenanceRequests()) {
			System.out.println(mr.toString());
		}
	}

	
	@Override
	public void listFacilityProblems() {
		// lists description of problems
		
		for(MaintenanceRequestInterface mr: this.getMaintenanceRequests()) {
			System.out.println( mr.getRequestor().getFacilityUser().getFirstName() +  "\t" + mr.getDateOfRequest() + "\t" + mr.getDescription() + "\t" + mr.getMaintainenceOrder().getStatus().toString());
		}
		
	}

}
