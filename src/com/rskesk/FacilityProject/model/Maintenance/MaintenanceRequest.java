package com.rskesk.FacilityProject.model.Maintenance;

import java.time.LocalDate;

import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUse;
import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUseInterface;


/**
 * Created by keetz on 2/9/17.
 */
public class MaintenanceRequest implements MaintenanceRequestInterface {
	private MaintenanceOrderInterface maintenanceOrder;
    private String description;
    private LocalDate dateOfRequest;	// Dates >> requested, scheduled, completed
    private boolean employeeAssigned;	// if true, change status to ACCEPTED
    // priority?
    
    private IndividualFacilityUseInterface requestor;
    
    public MaintenanceRequest(String description, IndividualFacilityUseInterface requestor){
    	this.description=description;
    	dateOfRequest=LocalDate.now();
    	maintenanceOrder=new MaintenanceOrder();
    	this.requestor = requestor;
    }


    public MaintenanceRequest() {
    	//blank constructor needed for bean
    	
	}
    

	public MaintenanceOrderInterface getMaintainenceOrder() {
		return maintenanceOrder;
	}

	public void setMaintenanceOrder(MaintenanceOrderInterface maintenanceOrder) {
		this.maintenanceOrder = maintenanceOrder;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public LocalDate getDateOfRequest() {
		return dateOfRequest;
	}

	public void setDateOfRequest(LocalDate dateOfRequest) {
		this.dateOfRequest = dateOfRequest;
	}
   
    public IndividualFacilityUseInterface getRequestor(){
    	return this.requestor;
    } 
    
    public void setRequestor(IndividualFacilityUseInterface r) {
    	this.requestor = r;
    }
    

}
