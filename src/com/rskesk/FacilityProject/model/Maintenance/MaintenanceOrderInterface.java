package com.rskesk.FacilityProject.model.Maintenance;

import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUse;
import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUseInterface;

public interface MaintenanceOrderInterface {
	public abstract CostInterface getCost();
	public abstract void setCost(CostInterface cost);
	public abstract MaintenanceStatus getStatus();
	public abstract void setStatus(MaintenanceStatus status);
	public abstract void setRequestor(IndividualFacilityUseInterface r) ;
	public abstract IndividualFacilityUseInterface getRequestor();
}
