package com.rskesk.FacilityProject.model.Maintenance;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUse;
import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUseInterface;

/**
 * Created by keetz on 2/10/17.
 */
public class MaintenanceOrder implements MaintenanceOrderInterface {
    private CostInterface cost;
    private MaintenanceStatus status;
    private IndividualFacilityUseInterface requestor;
    
    public MaintenanceOrder() {
       	cost = ((CostInterface) (new ClassPathXmlApplicationContext("META-INF/app-context.xml")).getBean("cost"));
       	
       	cost.setHoursOnJob((int) (Math.random() * 24));
       	cost.setOrderCost(Math.random() * 24);
       	cost.setSuppliesPurchased(Math.random() * 14);
    	status = MaintenanceStatus.PENDING;
    }
    
	public CostInterface getCost() {
		return cost;
	}

	public void setCost(CostInterface cost) {
		this.cost = cost;
	}

	public MaintenanceStatus getStatus() {
		if(this.status != null)
			return this.status;
		this.status = MaintenanceStatus.PENDING;
		return this.status;
	}

	public void setStatus(MaintenanceStatus status) {
		this.status = status;
	}
    
	public void setRequestor(IndividualFacilityUseInterface r) { 
		this.requestor = r;
	}
	
	public IndividualFacilityUseInterface getRequestor() {
		return this.requestor;
	}
}