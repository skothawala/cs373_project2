package com.rskesk.FacilityProject.model.Maintenance;

import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUse;
import com.rskesk.FacilityProject.model.Facility.User.IndividualFacilityUseInterface;

public interface Maintainable {
	
	void makeFacilityMaintRequest(String desc, IndividualFacilityUseInterface individualFacilityUseInterface);
	void acceptMaintenance(boolean empAssigned);
	void scheduleMaintenance();
	void completeMaintainence();
	double calcMaintenanceCostForOrder();
	double calcMaintenanceCostForFacility() ;
	double calcProblemRateForFacility();
	double calcDownTimeForFacility();
	void listMaintRequests();
	void listMaintenance();
	void listFacilityProblems();
}
